# Mini Plus Stock SD Card

This repository contains all the stock files from the Miyoo Mini Plus SD card, as it came from Miyoo directly. It does NOT, however, contain ANY copyrighted materials including ROMs and BIOS files. Copy the entirety of this repo to a fresh, FAT32 formatted SD card to reset your Mini Plus to Stock.